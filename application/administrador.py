__author__ = 'ardrete'

import re
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render

def home():
    datos = dict(seccion='Admnistracion', modulo='General', recurso='Home')
    return render ("Home", 'admin_home', 'back', **datos)

def pedidos():
    datos = dict(seccion='Admnistracion', modulo='Pedidos', recurso='Listado de Pedidos')
    return render ("Listado de pedidos", 'pedidos_listar', 'back', **datos)

def clientes():
    datos = dict(seccion='Admnistracion', modulo='Clientes', recurso='Listado de Clientes')
    return render ("Listado de Clientes", 'clientes_listar', 'back', **datos)

