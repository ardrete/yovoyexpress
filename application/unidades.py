import re
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render


def monitoreo():
    datos = dict(seccion='Administracion', modulo='Unidades', recurso='Monitoreo')
    template = render("Unidades", 'unidades_monitoreo', 'back', **datos)
    diccionario = dict(

    )
    return Template(template).safe_substitute(diccionario)