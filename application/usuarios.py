# -*- coding: utf-8 -*-
from flanker.addresslib import address
from hashlib import md5
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render


def registro(environ={}):
    dic= {
        'nombre':'',
        'apellidos':'',
        'telefono':'',
        'email':'',
        'password1':'',
        'password2':''
    }
    datos_form = {}
    if 'datos_form' in environ:
        datos_form = environ['datos_form']
    dic.update(datos_form)
    template = leer_archivo('registro', 'front')
    sustitucion = Template(template).safe_substitute(dic)
    datos = dict(seccion='Inicio', modulo='usuarios', recurso="registro")
    return render('', sustitucion, 'front', **datos)

@POSTDataHandler
def guardar(environ):
    POST = environ['_POST']
    nombre = POST['nombre'].upper()
    apellidos = POST['apellidos'].upper()
    telefono = POST['telefono'].replace(' ','') # numeros, min 8
    correo_saneado = sanear_correo(POST['email'])
    password1 = POST['password1']
    password2 = POST['password2']
    hash1 = md5(password1).hexdigest()
    hash2 = md5(password2).hexdigest()

    # sanear telefono
    tellist = []
    for caracter in telefono:
        if caracter.isdigit():
            tellist.append(caracter)
    telefono = ''.join(tellist)

    errores = {}

    #Si el formato del correo es invalido
    correo = address.validate_address(correo_saneado)

    if correo is None:
        errores['err_email'] = 'has-error'
        errores['leyenda'] = 'data-original-title'
        errores['err_icon_email'] = "fa fa-warning tooltips"

    #Si el correo esta duplicado
    else:
        query_duplicados = """
                            SELECT
                                    usuarios.email
                            FROM    usuarios
                            WHERE   usuarios.email = '{}'
                            """.format(correo)
        duplicados = run_query(query_duplicados)

        if len(duplicados) > 0:
            errores['err_email'] = "has-error"
            errores['err_icon_email'] = "fa fa-warning tooltips"
            errores['duplicado'] = "data-original-title"

    # telefono
    if telefono == '':
        errores['err_telefono'] = "has-error"
        errores['err_icon_telefono'] = "fa fa-warning tooltips"
    elif len(telefono) < 8:
        errores['err_telefono'] = "has-error"
        errores['err_icon_telefono'] = "fa fa-warning tooltips"
    elif not telefono.isdigit():
        errores['err_telefono'] = "has-error"
        errores['err_icon_telefono'] = "fa fa-warning tooltips"

    # Longitud menor a 6 digitos
    if len(password1) and len(password2) < 6:
        errores['err_password'] = 'has-error'
        errores['err_icon_password'] = 'fa fa-warning tooltips'
        errores['$passwordmenora6'] = "data-original-title"

    elif hash1 != hash2:
        errores['err_password'] = 'has-error'
        errores['err_icon_password'] = 'fa fa-warning tooltips'
        errores['passwornocoincide'] = "data-original-title"

    if not errores:
        sql = """
            INSERT INTO usuarios
            (email, password, nombre, apellido, telefono, nivel)
            VALUES
            ('{}', '{}', '{}', '{}', '{}', {} )
            """.format(correo, hash1, nombre, apellidos, telefono, 1)
        run_query(sql)
        diccionario = {
            'titulo': 'Gracias por registrarte, ahora puedes iniciar sesión'
        }
        template = leer_archivo('login', 'front')
        sustitucion = Template(template).safe_substitute(diccionario)
        datos = dict(seccion='Inicio', modulo='usuarios', recurso="registro")
        return render('', sustitucion, 'front', **datos)
    else:
        datos_form = POST.copy()
        datos_form.update(errores)
        environ['datos_form'] = datos_form
        return registro(environ)


def recuperar_contrasena():
    template = leer_archivo('recuperar_contrasena', 'front')
    datos = dict(seccion='Inicio', modulo='usuarios', recurso="registro")
    return render('', template, 'front', **datos)
