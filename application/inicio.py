# -*- coding: utf-8 -*-
from core.dblayer import run_query
from core.helpers import leer_archivo
from core.postdata import POSTDataHandler
from core.views import render


def contacto():
    template = leer_archivo('contacto','front')
    datos = dict(seccion='Inicio', modulo='contacto', recurso="ver")
    return render('Contáctanos', template, 'front', **datos)


def home():
    template = leer_archivo('index','front')
    datos = dict(seccion='', modulo='', recurso='')
    return render('', template, 'front', **datos)