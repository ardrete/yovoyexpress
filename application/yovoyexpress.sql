CREATE TABLE IF NOT EXISTS usuarios (
    usuario_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(100),
    password VARCHAR(100),
    nombre VARCHAR(100),
    apellido VARCHAR(100),
    telefono VARCHAR(12),
    nivel INT(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO usuarios (usuario_id, email, password, nombre, apellido, nivel, telefono)
VALUES (
    1, 'eng.ellery@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Ellery',
    'Eng', 1, 1234456789);

-- tabla codificadora
CREATE TABLE IF NOT EXISTS estados (
    estado_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO estados (estado_id, denominacion) VALUES (1, 'Pendiente');

CREATE TABLE IF NOT EXISTS pedidos (
    pedido_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    usuario INT(11),
        FOREIGN KEY (usuario)
        REFERENCES usuarios (usuario_id)
        ON DELETE SET NULL,
    fecha DATE,
    estado INT(11),
    FOREIGN KEY (estado)
        REFERENCES estados (estado_id)
        ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS pedidodetalle (
  pedidodetalle_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pedido INT(11),
    FOREIGN KEY (pedido)
    REFERENCES pedidos (pedido_id)
    ON DELETE CASCADE,
  nombre_origen VARCHAR(100) NOT NULL,
  direccion_mapa_origen VARCHAR(200) NOT NULL,
  lat_origen FLOAT(10,6),
  lon_origen FLOAT(10,6),
  referencia_origen VARCHAR(160),
  nombre_destino VARCHAR(100) NOT NULL,
  direccion_mapa_destino VARCHAR(200) NOT NULL,
  lat_destino FLOAT(10,6),
  lon_destino FLOAT(10,6),
  referencia_destino VARCHAR(160),
  banderazo DECIMAL(6, 2) NOT NULL,
  precio_kilometro DECIMAL(6, 2) NOT NULL,
  totalkm DECIMAL(6, 2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tarifas (
  tarifa_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  usuario INT(11),
    FOREIGN KEY (usuario)
    REFERENCES usuarios (usuario_id)
    ON DELETE SET NULL,
  banderazo DECIMAL(6, 2) NOT NULL,
  precio_kilometro DECIMAL(6, 2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO tarifas (tarifa_id, usuario, banderazo, precio_kilometro)
VALUES (1, 1, 40.00, 7.00)
