#-*- coding:utf-8 -*-
from sys import path
path.append(__file__.replace('/controller.py', ''))
from settings import PRIVATE_DIR


from beaker.middleware import SessionMiddleware


def front_controller(environ, start_response):
    pagina = environ['REQUEST_URI']
    content_type = 'text/html; charset=utf-8'
    modulo = 'inicio' #cualquier modulo de la app ejemplo salones
    recurso = 'home'
    argumento = ''

    # variables_de_sesion = ['user_id', 'logged', 'level', 'username', 'lastname', 'telefono' ]
    # for var in variables_de_sesion:
    #     if not var in environ['beaker.session']:
    #         environ['beaker.session'][var] = 0
    #
    # environ['beaker.session']['user_id'] = 1

    uri_elements = pagina.split('/')
    if len(uri_elements) == 3:
        null, modulo, recurso = uri_elements
    elif len(uri_elements) == 4:
        null, modulo, recurso, arg = uri_elements

    if modulo == "uploads":
        imagen = "%s/%s" % (PRIVATE_DIR, recurso)
        with open(imagen, 'r') as archivo:
            salida = archivo.read()
        encabezados = [('Content-Type', 'image/jpeg')]
    else:
        exec "from %s import %s" % (modulo, recurso)
        try:
            salida = locals()[recurso](environ)
        # except Exception as e:
        #     salida = str(e)
        except:
            salida = locals()[recurso]()

        encabezados = [('Content-Type', content_type)]
        if isinstance(salida, tuple):
            valor_refresh = '%i; url=%s' % (salida[0], salida[1])
            http_redirect = ('Refresh', valor_refresh)
            encabezados.append(http_redirect)
            salida = ''

    # Enviar rta a WSGI
    start_response('200 OK', encabezados)
    return salida


# el middleware tiene que actuar ANTES de application.
beaker_dic = {
    'session.type': 'file',
    'session.cookie_expires': True,
    'session.auto': True,
    'session.data_dir': '/tmp/sessions',
    }

application = SessionMiddleware(front_controller, beaker_dic)