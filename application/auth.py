from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render

def signIn():
    datos = dict(seccion='Home', modulo='Auth', recurso='SignIn')
    return render ("Sign In", 'auth_signIn', 'auth', **datos)

def isAuth():
    return True
