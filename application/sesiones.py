# -*- coding: utf-8 -*-
from hashlib import md5
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo
from core.postdata import POSTDataHandler
from core.views import render


def ingresar():
    diccionario = {
        'titulo': 'Iniciar Sesión'
    }
    template = leer_archivo('login', 'front')
    sustitucion = Template(template).safe_substitute(diccionario)
    datos = dict(seccion='Inicio', modulo='sesiones', recurso="iniciar")
    return render('', sustitucion, 'front', **datos)

def acceso():
    template = leer_archivo('acceso', 'front')
    datos = dict(seccion='Inicio', modulo='acceso', recurso="iniciar")
    return render('', template, 'front', **datos)

def salir(environ):
    __destruir(environ)
    return (0, '/inicio/home')


@POSTDataHandler
def validar(environ):
    _POST = environ['_POST']

    # Datos POST
    email = _POST['user']
    clave = md5(_POST['clave']).hexdigest()
    uri = environ['HTTP_REFERER']

    sql = """SELECT usuario_id, email, password, nivel, nombre, apellido, telefono
        FROM usuarios WHERE email = "%s" AND password = "%s"
    """ % (email, clave)

    datos = run_query(sql)
    if len(datos) > 0:
        environ['beaker.session']['logged'] = True
        environ['beaker.session']['user_id'] = datos[0][0]
        environ['beaker.session']['level'] = datos[0][3]
        environ['beaker.session']['username'] = datos[0][1]
        environ['beaker.session']['name'] = datos[0][4]
        environ['beaker.session']['lastname'] = datos[0][5]
        environ['beaker.session']['telefono'] = datos[0][6]
        return (0, '/pedidos/agregar')
    else:
        __destruir(environ)
        return ingresar()


def __destruir(environ):
    environ['beaker.session']['logged'] = False
    environ['beaker.session']['user_id'] = 0
    environ['beaker.session']['level'] = 0
    environ['beaker.session']['username'] = 'Anonimous'
    environ['beaker.session'].delete()


def verificar(funcion_restringida):

    def wrapper(*args, **kwargs):
        environ = args[0]
        esta_logueado = False

        if 'logged' in environ['beaker.session']:
            if environ['beaker.session']['logged'] is True:
                esta_logueado = True
                return funcion_restringida(*args, **kwargs)

        if not esta_logueado:
            __destruir(environ)
            return acceso()

    return wrapper