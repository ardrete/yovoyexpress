# -*- coding: utf-8 -*-
import re
import smtplib
import decimal
from time import strftime
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render
from sesiones import verificar

@verificar
def agregar(environ={}):
    user_id = int(environ['beaker.session']['user_id'])
    name = environ['beaker.session']['name']
    lastname = environ['beaker.session']['lastname']
    username = environ['beaker.session']['username']
    telefono = environ['beaker.session']['telefono']


    dic = {
    'user_id': user_id,
    'nombre': name,
    'apellidos': lastname,
    'telefono': telefono,
    'email': username,
    'nombreRemitente': '',
    'banderazo': 40.00,
    'precioKilometro':7.00
    }

    datos_form = {}
    if 'datos_form' in environ:
        datos_form = environ['datos_form']
    dic.update(datos_form)

    # #Tarifas
    #
    # sql = """
    #     SELECT
    #             banderazo,
    #             precio_kilometro
    #     FROM
    #             tarifas
    #     WHERE
    #             usuario = {}
    # """.format(1)
    # data = run_query(sql)
    # return str(data)

    form=leer_archivo('pedidos_agregar', 'front')
    template = Template(form).safe_substitute(dic)
    datos = dict(seccion='Clientes', modulo='Pedidos', recurso="Agregar Pedido")
    return render('Agregar pedido', template, 'front', **datos)


@POSTDataHandler
def guardar(environ):
    POST = environ['_POST']

    #Datos del pedido
    id_usuario = int(environ['beaker.session']['user_id'])
    fecha = strftime("%Y-%m-%d")
    estado = 1

    #Datos de la direccion de origen
    nombre_origen = POST['nombreRemitente']
    direccion_mapa_origen = POST['direccionMapaOrigen']
    lat_origen = float(POST['latitudOrigen'])
    lon_origen = float(POST['longitudOrigen'])
    referencia_origen = POST['referenciaOrigen']

    #Datos de la direccion de destino
    nombre_destino = POST['nombreDestino']
    direccion_mapa_destino = POST['direccionMapaDestino']
    lat_destino = float(POST['latitudDestino'])
    lon_destino = float(POST['longitudDestino'])
    referencia_destino = POST['referenciaDestino']

    #Datos de tarifa
    banderazo = float(POST['banderazo'])
    precio_kilometro = float(POST['tarifa'])
    totalkm = float(POST['kmTotalesString'])

    #guardar datos del pedido
    sql = """
        INSERT INTO pedidos
        (usuario, fecha, estado)
        VALUES
        ({}, '{}', {})
    """.format(id_usuario, fecha, estado)

    pedido_id = run_query(sql)
    #Guardar datos detalles del pedido
    query = """
            INSERT INTO pedidodetalle
            (pedido, nombre_origen, direccion_mapa_origen, lat_origen,
            lon_origen, referencia_origen, nombre_destino,
            direccion_mapa_destino, lat_destino, lon_destino, referencia_destino,
            banderazo, precio_kilometro, totalkm)
            VALUES
            ({}, '{}', '{}', {}, {}, '{}', '{}', '{}', {}, {}, '{}', {}, {}, {})
         """.format(pedido_id, nombre_origen, direccion_mapa_origen, lat_origen,
                    lon_origen, referencia_origen, nombre_destino,
                    direccion_mapa_destino, lat_destino, lon_destino, referencia_destino,
                    banderazo, precio_kilometro, totalkm)

    pedido_detalle_id = run_query(query)




    #Armando el correo electronico
    # nombre = environ['beaker.session']['name']
    # remitente = "Desde Vialexpress <contacto@vialexpress.com>"
    # destinatario = environ['beaker.session']['username']
    # asunto = "Pedido de VialExpress"
    # mensaje = """
    #         Hola! <br>
    #         Tu hemos recibido tu pedido<br>
    #         Numero de guia: %s
    #         Nombre del solicitante: %s
    #         Nombre del remitente: %s
    #         Direccion de recoleccion: %s
    #         Destinatario: %s
    #         Direccion de entrega: %s
    #         Descripcion: %s
    #         Ditancia: %s
    #         Precio: %s
    #         """ % (pedido_id, nombre, nombre_origen, direccion_mapa_origen,
    #                    nombre_destino, direccion_mapa_destino, referencia_origen,
    #                    totalkm)
    # email = """
    #         From: %s
    #         To: %s
    #         MIME-Version: 1.0
    #         Content-type: text/html
    #         Subject: %s
    #         %s
    #         """ % (remitente, destinatario, asunto, mensaje)
    # a = """El correo fue enviado con exito"""
    # b = """El mensaje no pudo enviarse ya que el sistema no esta preparado"""
    #
    # try:
    #     smtp = smtplib.SMTP('localhost')
    #     smtp.sendmail(remitente, destinatario, email)
    #     return str(a)
    # except:
    #     return str(b)

    return (0, "/pedidos/ver/{}".format(pedido_id))


def ver(environ):
    try:
        pedidoid = int(environ['REQUEST_URI'].split('/')[-1])
    except:
        pedidoid = 0

    #Datos del pedido
    sql = """
        SELECT
                usuarios.nombre,
                usuarios.apellido,
                pedidos.fecha,
                pedidos.pedido_id
        FROM    pedidos INNER JOIN usuarios ON pedidos.usuario = usuarios.usuario_id
        WHERE   pedidos.pedido_id = {}
    """.format(pedidoid)

    data = run_query(sql)

    for datos in data:
        diccionario = {
            'nombre':datos[0],
            'apellido' : datos[1],
            'fecha': datos[2],
            'guia': datos[3]
        }
    # Render con los datos del pedido
    plantilla = leer_archivo('pedidos_ver', 'front')
    sustitucion = Template(plantilla).safe_substitute(diccionario)

    #Detalles del pedido
    sql = """
        SELECT
                pedidodetalle.nombre_origen,
                pedidodetalle.direccion_mapa_origen,
                pedidodetalle.referencia_origen,
                pedidodetalle.nombre_destino,
                pedidodetalle.direccion_mapa_destino,
                pedidodetalle.referencia_destino,
                pedidodetalle.totalkm,
                pedidodetalle.precio_kilometro,
                pedidodetalle.banderazo
        FROM    pedidodetalle
        WHERE   pedidodetalle.pedido = {}
        """.format(pedidoid)

    detallepedido = run_query(sql)

    for resultado in detallepedido:
        detallesdelpedido = {
            'nombre_origen': resultado[0],
            'direccion_mapa_origen': resultado[1],
            'referencia_origen': resultado[2],
            'nombre_destino': resultado[3],
            'direccion_mapa_destino': resultado[4],
            'referencia_destino': resultado[5],
            'totalkm': resultado[6],
            'subtotal': round(float(resultado[8] + (resultado[6] * resultado[7]))/1.16,2),
            'iva': round(float(resultado[8] + (resultado[6] * resultado[7]))*.16,2),
            'total': round(float(resultado[8] + (resultado[6] * resultado[7]))/1.16,2) + round(float(resultado[8] + (resultado[6] * resultado[7]))*.16,2)
        }

    render_final = Template(sustitucion).safe_substitute(detallesdelpedido)
    datos = dict(seccion='Clientes', modulo='pedidos', recurso='ver')
    return render ('', render_final, 'front', **datos)


def editar(idGuia=0):
    dic = {
        'nombre': '',
        'apellidoPaterno': '',
        'apellidoMaterno': '',
        'telefonoFijo': '',
        'telefonoMovil': '',
        'email': ''
    }
    # dic.update(datos_form)
    template = leer_archivo('pedidos_editar', 'front')
    datos = dict(seccion='Clientes', modulo='Pedidos', recurso="Consultar Pedido")
    result = render("Consultar pedido", template, 'front', **datos)
    return Template(result).safe_substitute(dic)

def test():
    datos = dict(seccion=' ', modulo=' ', recurso="Test")
    return render("Test", 'test', 'front', **datos)


