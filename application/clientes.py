import re
from string import Template
from core.dblayer import run_query
from core.helpers import leer_archivo, sanear_correo
from core.postdata import POSTDataHandler
from core.views import render


def actualizar(idCliente):
    datos = dict(seccion='Admnistracion', modulo='Clientes', recurso='Actualizar')
    template = render("Actualizar Cliente", 'clientes_actualizar', 'back', **datos)
    diccionario = dict(
        nombre="",
        apellidoMaterno="",
        apellidoPaterno="",
        telefonoFijo="",
        telefonoMovil="",
        email="",
        password="",
        nombreEmpresa="",
        razonSocial="",
        rfc="",
        esDireccionFiscal="",
        calle="",
        numero="",
        colonia="",
        codigoPostal="",
        municipioDelegacion="",
        referencia="",
        calleFiscal="",
        numeroFiscal="",
        coloniaFiscal="",
        codigoPostalFiscal="",
        municipioDelegacionFiscal="",
        referenciaFiscal=""
    )
    return Template(template).safe_substitute(diccionario)

def editar(idCuenta):
    datos = dict(seccion='Clientes', modulo='Home', recurso='Editar')
    template = render("Editar Cuenta", 'cliente_editar', 'front', **datos)
    diccionario = dict(
        nombre="",
        apellidoMaterno="",
        apellidoPaterno="",
        telefonoFijo="",
        telefonoMovil="",
        email="",
        password="",
        nombreEmpresa="",
        razonSocial="",
        rfc="",
        esDireccionFiscal="",
        calle="",
        numero="",
        colonia="",
        codigoPostal="",
        municipioDelegacion="",
        referencia="",
        calleFiscal="",
        numeroFiscal="",
        coloniaFiscal="",
        codigoPostalFiscal="",
        municipioDelegacionFiscal="",
        referenciaFiscal=""
    )
    return Template(template).safe_substitute(diccionario)


def resumen(environ):
    diccionario = dict(
        NombreCompleto="Juan Velazquez",
        Empresa="Sembei",
        GuiasDisponibles = 230,
        GuiasUtilizadas = 20,
        KmRecorridos = 20
    )
    template = leer_archivo('cliente_resumen', 'front')
    sustitucion = Template(template).safe_substitute(diccionario)
    datos = dict(seccion='Clientes', modulo='Home', recurso='Resumen')
    return render ("Resumen", sustitucion, 'front', **datos)



def guiasUtilizadas(environ):
    template = leer_archivo('cliente_guiasUtilizadas', 'front')
    datos = dict(seccion='Clientes', modulo='Home', recurso='Guias')
    return render("Guias", template, 'front', **datos)


def tarifas():
    dic = dict(
        Banderazo=  40,
        Km = 3.5,
        TarifaFija = 90,
        Descuento = 10
    )
    form = leer_archivo('clientes_tarifas', 'back')
    template = Template(form).safe_substitute(dic)
    datos = dict(seccion='Administracion', modulo='Tarifas', recurso='Consultar')
    return render('Tarifas', template, 'back', **datos)
