import sys
version = sys.version
path = sys.path
def application(environ, start_response):
    status = '200 OK'
    output = 'Hello World! ' + '\n Python: ' + version + '\n Path: ' ' '.join(path)
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]