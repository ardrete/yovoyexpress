from string import Template
from core.helpers import leer_archivo


# Setar encabezados HTTP
def setear_encabezados_http():
    cabeceras = []  #Las cabeceras que se van a modificar
    encabezados = [("content-type", "text/html; charset=utf-8")]
    cabeceras.append(encabezados)
    return encabezados


# Renderizar template
def render(titulo, contenido, nombre_carpeta, seccion='', modulo='', recurso=''):
    plantilla = leer_archivo('template', nombre_carpeta)
    diccionario = dict(
        TITULO=titulo,
        CONTENIDO=contenido,
        SECCION=seccion,
        MODULO=modulo,
        RECURSO=recurso
    )
    salida = Template(plantilla).safe_substitute(diccionario)
    return salida