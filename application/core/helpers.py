#-*- coding: utf-8 -*-
from cgi import escape

from settings import STATIC_DIR as directorio


# Leer archivo
def leer_archivo(filename, nombre_carpeta):
    archivo = '%(directorio)s/%(carpeta)s/%(template)s.html' % dict(directorio=directorio,
            carpeta=nombre_carpeta, template=filename)

    with open(archivo, 'r') as pagina:
        contenido = pagina.read()

    return contenido

def escapar(cadena):
    """convertir caracteres extraños en entidades HTML"""
    resultado = escape(cadena, quote=True).replace("'", "")
    return resultado


def sanear_correo(correo):
    nuevo_correo = correo
    caracteres_permitidos = ["@", ".", "-", "_", "+"]
    for caracter in correo:
        if not caracter.isalnum() and not caracter in caracteres_permitidos:
            nuevo_correo = nuevo_correo.replace(caracter, "")
    return nuevo_correo