# -*- coding: utf-8 -*-
from urllib2 import unquote
# sirve para almacenar una secuencia de campos leyendo, justamente, los datos
# enviados desde un formulario codificado como multipart/form-data
from cgi import FieldStorage
from tempfile import TemporaryFile


def __get_simple_form_data(env):
    datos = env['wsgi.input'].read().split('&')
    # Como el delimitador de campos es el signo &, podemos obtener una lista
    # donde cada elemento sea un par clave=valor

    POST = {}

    for par in datos:
        campo, valor = par.split('=')
        # dividimos la lista
        valor = unquote(valor).replace('+', ' ')
        # sacamos los caracteres que no nos sirven

        if not campo in POST:
        #chequeamos si no hay campos repetidos, ya que si hay campos repetidos
        #significa que hay radio buttons y tenemos que agruparlos
            POST[campo] = valor
        else:
            if not isinstance(POST[campo], list): # aún no es una lista
                POST[campo] = [POST[campo], valor]
            else:
            # ya es una lista
                POST[campo].append(valor)
    return POST


# para la carga de archivos el form debe tener el enctype multipart/form-data,
# que no es el que viene por defecto que es application/x-www-form-urlencoded,
# que sirve para leer formularios planos
def __get_complex_form_data(env):
    _POST = {}

    # Crea un archivo temporal y lo abre (por defecto) en modo w+r
    archivo_temporal = TemporaryFile()
    # Escribimos el contenido de wsgi.input en el archivo temporal
    archivo_temporal.write(env['wsgi.input'].read())
    # Movemos al cursor al inicio ya que necesitaremos leer este archivo desde
    archivo_temporal.seek(0)

    #se lo pasamos a WSGI y se convierte en un diccionario
    datos = FieldStorage(fp=archivo_temporal, environ=env)

    # Finalmente, solo resta utilizar las claves y valores para crear
    # nuestro propio diccionario
    for fieldname in datos:
        formfield = datos[fieldname]
        if isinstance(formfield, list):
        # Si _POST['imagen'] es una lista, significa que hay mas de un archivo,
            _POST[fieldname] = []
            # entonces iteras con for
            for elemento in formfield:
                _POST[fieldname].append(__get_value(elemento))
        else:
            _POST[fieldname] = __get_value(formfield)

    return _POST


def __get_value(elemento):
    archivo = dict(filetype=elemento.type, filename=elemento.filename,
            content=elemento.value)
    return elemento.value if elemento.filename is None else archivo


# podemos crear un decorador que envuelve cada funcion que reciba datos de un
# formulario
def get_post_data(funcion):

    def wrapper(*args):
        _POST = {}

        try:
            if not args[0]['CONTENT_TYPE'].startswith('multipart/form-data'):
                _POST = __get_simple_form_data(args[0])
            else:
                _POST = __get_complex_form_data(args[0])
        except Exception as error:
            _POST['error'] = error
        finally:
            fargs = [_POST if n == 0 else args[n] for n in range(0, len(args))]

        return funcion(*fargs)

    return wrapper
