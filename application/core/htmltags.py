# -*- coding: utf-8 *-*
"""Etiquetas HTML para creación de tablas"""

__tbl_open = "<table>%s" % chr(10)
__tbl_close = "</table>"
__tr_open = "<tr>"
__tr_close = "</tr>%s" % chr(10)
__th_open = "<th>"
__th_close = "</th>"
__td_open = "<td>"
__td_close = "</td>"


TABLE_TAGS = dict(tblopen=__tbl_open,
    tblclose=__tbl_close,
    tropen=__tr_open,
    trclose=__tr_close,
    thopen=__th_open,
    thclose=__th_close,
    tdopen=__td_open,
    tdclose=__td_close)
