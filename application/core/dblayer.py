# -*- coding: utf-8 *-*
import MySQLdb

from settings import DB_HOST, DB_USER, DB_PASS, DB_NAME


def run_query(query):
    datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME]
    conn = MySQLdb.connect(*datos)
    cursor = conn.cursor()
    cursor.execute(query)

    if _limpiar(query).startswith('SELECT'):
        data = cursor.fetchall()
    else:
        conn.commit()
        if _limpiar(query).startswith('INSERT'):
            data = cursor.lastrowid
        else:
            data = None

    cursor.close()
    conn.close()

    return data

def _limpiar(query):
    return query.upper().replace(' ', '').replace('\n', '')