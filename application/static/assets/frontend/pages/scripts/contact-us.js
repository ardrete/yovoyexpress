var ContactUs = function () {

    return {
        //main function to initiate the module
        init: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
	            lat: 19.3428458,
				lng: -99.050171,
			  });
			   var marker = map.addMarker({
		            lat: 19.2866419,
					lng: -99.0697767,
		            title: 'Vial Express SA de CV.',
		            infoWindow: {
		                content: "<b>Vial Express.</b> Vada 26 Colonia Rinconada del Molino<br>Del. Iztapalapa"
		            }
		        });

			   marker.infoWindow.open(map, marker);
			});
        }
    };

}();