"use strict";
var Extensions = function () {
    var preventDefaultEvent = function (event) {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    }

    return {
        init: function () {
            String.prototype.format = function () {
                var pattern = /\{\d+\}/g;
                var args = arguments;
                return this.replace(pattern, function (capture) {
                    return args[capture.match(/\d+/)];
                });
            };

            jQuery.fn.outerHTML = function (s) {
                return s
                    ? this.before(s).remove()
                    : jQuery("<p>").append(this.eq(0).clone()).html();
            };

            Number.prototype.toMoney = function (decimals, decimal_sep, thousands_sep) {
                var j;
                var n = this,
                    c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
                    d = decimal_sep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)

                /*
                 according to [http://stackoverflow.com/questions/411352/how-best-to-determine-if-an-argument-is-not-sent-to-the-javascript-function]
                 the fastest way to check for not defined parameter is to use typeof value === 'undefined'
                 rather than doing value === undefined.
                 */
                    t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value

                    sign = (n < 0) ? '-' : '',

                //extracting the absolute value of the integer part of the number and converting to string
                    i = parseInt(n = Math.abs(n).toFixed(c)) + '',

                    j = ((j = i.length) > 3) ? j % 3 : 0;
                return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
            };

            Number.prototype.format = function(lengthDecimals, lengthWholePart, sectionDelimiter, decimalDelimiter) {
                var re = '\\d(?=(\\d{' + (lengthWholePart || 3) + '})+' + (lengthDecimals > 0 ? '\\D' : '$') + ')',
                    num = this.toFixed(Math.max(0, ~~lengthDecimals));

                return (decimalDelimiter ? num.replace('.', decimalDelimiter) : num).replace(new RegExp(re, 'g'), '$&' + (sectionDelimiter || ','));
            };

            try {
                $.fn.dataTable.moment('DD/MM/YYYY H:m');
            }
            catch (err) {
                console.log("In this page not are using datatables");
            }
        },
        preventDefault: preventDefaultEvent
    }
}();