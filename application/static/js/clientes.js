var Clientes = function () {

    var $calendar = jQuery("#calendarGuias");

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    };


    var initPopovers = function () {
        var $calendar = jQuery("#calendarGuias");

        $calendar.popover({
            animation: true,
            html: true,
            placement: "auto top",
            title: "Detalle Pedido",
            trigger: "focus hover",
            selector: ".fc-content",
            container: "body"
        });

        $calendar.on("show.bs.popover", onPopoverShow)


    };

    var onEventRender = function (event, element) {
        element.data("id-fc", event._id);
        element.attr("href", "/pedidos/editar/{0}".format(event.idGuia))
    };

    var onPopoverShow = function (event) {
        event = event || window.event;
        var $target = jQuery(event.target);


        var idFullCalendar = $target.closest(".fc-event").data("id-fc");

        var dataEvent = $calendar.fullCalendar("clientEvents", idFullCalendar)[0];

        var htmlContent = "<div class='row'>" +
            "<p>Nombre Contacto: {0}</p>".format(dataEvent.nombreContacto) +
            "<p>Fecha Inicio: {0}</p>".format(dataEvent.fechaInicio) +
            "<p>Fecha Recibido: {0}</p>".format(dataEvent.fechaRecepcion) +
            "<p>Fecha Entrega: {0}</p>".format(dataEvent.fechaEntrega) +
            "</div>";


        var $eventContent = $target.closest(".fc-content");
        $eventContent.removeAttr("data-content");
        $eventContent.attr("data-content", htmlContent);

    };


    var loadEvents = function (start, end, timezone, callback) {

        var $requestEvents = $.ajax({
            url: "/static/jsonData/eventosGuias.json",
            dataType: "json",
            type: "GET"
        });

        $requestEvents.fail(function () {
            alert("No se pudieron obtener la calendarizacion de eventos.")
        });
        $requestEvents.done(function (data) {
            if (data && $.isArray(data.events)) {
                callback(data.events);
            }
        });

    };


    var loadCalendar = function () {
        if (!jQuery().fullCalendar) {
            return;
        }

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var header = {};

        var $calendar = $("#calendarGuias");

        if ($calendar.width() <= 400) {
            $calendar.addClass("mobile");
            header = {
                left: 'title, prev, next',
                center: '',
                right: 'today,month,agendaWeek,agendaDay'
            };
        } else {
            $calendar.removeClass("mobile");
            if (Metronic.isRTL()) {
                header = {
                    right: 'title',
                    center: '',
                    left: 'prev,next,today,month,agendaWeek,agendaDay'
                };
            } else {
                header = {
                    left: 'title',
                    center: '',
                    right: 'prev,next,today,month,agendaWeek,agendaDay'
                };
            }
        }


        $calendar.fullCalendar('destroy'); // destroy the calendar
        $calendar.fullCalendar({ //re-initialize the calendar
            disableDragging: true,
            header: header,
            editable: false,
            events: loadEvents,
            eventRender: onEventRender
        });


    };


    var handleClientes = function () {

        var $tableClientes = jQuery("#dataTableClientes");

        /* Formatting function for row details */
        function fnFormatDetails(oTable, nTr) {
            var aData = oTable.fnGetData(nTr);
            var sOut = '<table>';
            sOut += '<tr><td>Telefono Fijo:</td><td>{0} ext {1}</td></tr>'.format(aData.telefonoFijo, aData.extension);
            sOut += '<tr><td>Celular:</td><td>{0}</td></tr>'.format(aData.celular);
            sOut += '<tr><td>Direccion:</td><td>' + aData.direccion + '</td></tr>';
            sOut += '<tr><td>Razon Social:</td><td>' + aData.razonSocial + '</td></tr>';
            sOut += '<tr><td>Password:</td><td>' + aData.password + '</td></tr>';
            sOut += '</table>';
            return sOut;
        }

        var oTable = $tableClientes.dataTable({ // here you can define a typical datatable settings from http://datatables.net/usage/options
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            pageLength: 10, // default record count per page
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                {
                    "className": '',
                    "orderable": false,
                    "data": "",
                    "defaultContent": "<span class='row-details row-details-close'></span>"
                },
                {"data": "idCliente"},
                {"data": "nombreCompleto"},
                {data: "empresa"},
                {data: "email"},
                {
                    data: "tipoCliente",
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var $select = $('<select name="order_status" class="form-control form-filter input-sm"> ' +
                        '     <option value="">Selecciona...</option>   ' +
                        '     <option value="1">Entrega Inmediata</option>     ' +
                        '     <option value="2">Empresarial</option>      ' +
                        ' </select>');
                        $select.find('option[value={0}]'.format(data)).attr('selected', true)
                        return $select.outerHTML();
                    }
                },
                {
                    "className": '',
                    "orderable": false,
                    "data": "idCliente",
                    "render": function (data, type, row, meta) {
                        return '<a class="btn btn-sm red filter-cancel" href="/clientes/actualizar/{0}"><i class="fa fa-times"></i>Cancelar</a> '.format(data) +
                            ' <a class="btn btn-sm yellow filter-cancel" href="/clientes/actualizar/{0}"><i class="fa fa-times"></i>Actualizar</a>'.format(data);
                    }
                }
            ],
            ajax: {
                url: "/static/jsonData/clientesAdmin.json", // ajax source
                type: "GET"
            },
            order: [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });


        var tableWrapper = $('#sample_3_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        $tableClientes.on('click', ' tbody td .row-details', function () {
            var nTr = $(this).parents('tr')[0];
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                $(this).addClass("row-details-close").removeClass("row-details-open");
                oTable.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).addClass("row-details-open").removeClass("row-details-close");
                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
            }
        });


    };

    var handleGuias = function () {

        var $tableGuias = jQuery("#dataTableGuiasUtilizadas");

        /* Formatting function for row details */
        function fnFormatDetails(oTable, nTr) {
            var aData = oTable.fnGetData(nTr);
            var sOut = '<table>';
            sOut += '<tr><td>Direccion Origen:</td><td>{0}</td></tr>'.format(aData.direccionOrigen);
            sOut += '<tr><td>Direccion Destino:</td><td>{0}</td></tr>'.format(aData.direccionDestino);
            sOut += '<tr><td>Telefono Contacto:</td><td>{0} ext {1}</td></tr>'.format(aData.telefono, aData.extension);
            sOut += '<tr><td>Celular Contacto:</td><td>{0}</td></tr>'.format(aData.celular);
            sOut += '</table>';
            return sOut;
        }

        var oTable = $tableGuias.dataTable({ // here you can define a typical datatable settings from http://datatables.net/usage/options
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            pageLength: 10, // default record count per page
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                {
                    "className": '',
                    "orderable": false,
                    "data": "",
                    "defaultContent": "<span class='row-details row-details-close'></span>"
                },
                {data: "idGuia"},
                {data: "nombreContacto"},
                {data: "fechaInicio"},
                {data: "fechaRecibido"},
                {data: "fechaEntrego"},
                {data: "kmRecorridos"}
            ],
            ajax: {
                url: "/static/jsonData/clientesGuiasUtilizadas.json", // ajax source
                type: "GET"
            },
            order: [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });


        var tableWrapper = $('#sample_3_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        $tableGuias.on('click', ' tbody td .row-details', function () {
            var nTr = $(this).parents('tr')[0];
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                $(this).addClass("row-details-close").removeClass("row-details-open");
                oTable.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).addClass("row-details-open").removeClass("row-details-close");
                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
            }
        });


    }

    var showPricingModal = function (event) {
        jQuery("#pricesModal").modal("show");
    };

    var initPricing = function () {
        jQuery("#ancPrices").click(showPricingModal)
    };
    return {
        consultar: function () {
            initPickers();
            handleClientes();
        },
        resumen: function () {
            initPopovers();
            initPricing();
            loadCalendar();

        },
        consultarGuiasUtilizadas: function () {
            handleGuias();
        }


    }
}();