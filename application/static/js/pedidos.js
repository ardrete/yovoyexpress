"use strict"
var Pedidos = function () {
    var mapModal;
    var optionsMarker;
    var contextAgregar = {
        mapOrigen: null,
        mapDestino: null,
        polygon: null
    };

    var contextEditar = {
        mapRastreo: null,
        route: null
    };

    var onMarkerClick = function (event) {
        event = event || window.event;
        var $target = jQuery(event.target);
        var data = $target.data();

        optionsMarker = {
            lat: data.lat,
            lng: data.lng
        };

        mapModal.removeMarkers();
        mapModal.addMarker(optionsMarker);
        mapModal.setCenter(optionsMarker.lat, optionsMarker.lng, null);

        jQuery("#mapModal").modal("show");
    };


    var handleMapModals = function () {
        mapModal = initMap("#map");
        jQuery("#dataTablePedidos").on("click", ".fa-map-marker", onMarkerClick);
        jQuery("#mapModal").on("shown.bs.modal", function () {
            mapModal.refresh();
            mapModal.setCenter(optionsMarker.lat, optionsMarker.lng, null);
        });
    };

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    };


    var handlePedidos = function () {

        var $tablePedidos = jQuery("#dataTablePedidos");

        /* Formatting function for row details */
        function fnFormatDetails(oTable, nTr) {
            var dataRow = oTable.fnGetData(nTr);
            var htmlOut = '<table>';
            htmlOut += '<tr><td>Origen:</td><td> {0}  <i id="mapOrigen{3}" class="fa fa-map-marker fa-2x" data-lat="{1}" data-lng="{2}"></i></td></tr>'.format(dataRow.direccionOrigen, dataRow.coordenadaOrigen.lat, dataRow.coordenadaOrigen.lng, dataRow.idGuia);
            htmlOut += '<tr><td>Destino:</td><td> {0} <i id="mapDestino{3}" class="fa fa-map-marker fa-2x" data-lat="{1}" data-lng="{2}"></i></td></tr>'.format(dataRow.direccionDestino, dataRow.coordenadaDestino.lat, dataRow.coordenadaDestino.lng, dataRow.idGuia);
            htmlOut += '<tr><td>Conductor:</td><td>' + dataRow.conductor + '</td></tr>';
            htmlOut += '<tr><td>Unidad:</td><td>' + dataRow.unidad + '</td></tr>';
            htmlOut += '</table>';
            return htmlOut;
        }

        var oTable = $tablePedidos.dataTable({ // here you can define a typical datatable settings from http://datatables.net/usage/options
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            pageLength: 10, // default record count per page
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                {
                    "className": '',
                    "orderable": false,
                    "data": "",
                    "defaultContent": "<span class='row-details row-details-close'></span>"
                },
                {"data": "idGuia"},
                {"data": "cliente"},
                {data: "fechaCreacion"},
                {data: "fechaEntrega"},
                {"data": "tiempoTranscurrido", type: "numeric"},
                {"data": "kmRecorridos", type: "numeric"},
                {
                    data: "idEstatus",
                    orderable: false,
                    render: function (data, type, row, meta) {
                        var $select = $('<select name="order_status" class="form-control form-filter input-sm"> ' +
                        '     <option value="">Selecciona...</option>   ' +
                        '     <option value="1">Canceladas</option>     ' +
                        '     <option value="2">Esperando</option>      ' +
                        '     <option value="3">Cerradas</option>       ' +
                        ' </select>');
                        $select.find('option[value={0}]'.format(data)).attr('selected', true)
                        return $select.outerHTML();
                    }
                },
                {
                    "className": '',
                    "orderable": false,
                    "data": "idGuia",
                    "render": function (data, type, row, meta) {
                        return '<a class="btn btn-sm red filter-cancel" href="/pedidos/cancelar/{0}"><i class="fa fa-times"></i>Cancelar</a>'.format(data);
                    }
                }
            ],
            ajax: {
                url: "/static/jsonData/pedidosAdmin.json", // ajax source
                type: "GET"

            },
            order: [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });


        var tableWrapper = $('#sample_3_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* Add event listener for opening and closing details
         * Note that the indicator for showing which row is open is not controlled by DataTables,
         * rather it is done here
         */
        $tablePedidos.on('click', ' tbody td .row-details', function () {
            var nTr = $(this).parents('tr')[0];
            if (oTable.fnIsOpen(nTr)) {
                /* This row is already open - close it */
                $(this).addClass("row-details-close").removeClass("row-details-open");
                oTable.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).addClass("row-details-open").removeClass("row-details-close");
                oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
            }
        });


    }


    var initMap = function (idSelector, title) {
        var map = new GMaps({
            div: idSelector,
            lat: -13.004333,
            lng: -38.494333
        });
        return map;
    };


    var getDireccionOrigen = function () {
        var direccion = jQuery("#direccionOrigen").val();
        return direccion;
    }


    var getDireccionDestino = function () {
        var direccion = jQuery("#direccionDestino").val();
        return direccion;
    };


    var getLatLngOrigen = function () {
        return new google.maps.LatLng($("#latitudOrigen").val(), $("#longitudOrigen").val());
    };

    var getLatLngDestino = function () {
        return new google.maps.LatLng($("#latitudDestino").val(), $("#longitudDestino").val());
    };

    function updateKmTotal() {
        if ($("#latitudDestino").val() && $("#latitudOrigen").val()) {
            var kmTotalesString = (google.maps.geometry.spherical.computeDistanceBetween(getLatLngOrigen(), getLatLngDestino()) / 1000).toFixed(2);
            var banderazo = parseFloat($("#banderazo").val());
            var tarifa = parseFloat($("#tarifa").val());
            var kmTotales = parseFloat(kmTotalesString);
            var amountTotal =banderazo + (kmTotales * tarifa);
            $("#divKmRuta").text(kmTotalesString);
            $("#kmTotalesString").val(kmTotalesString);

            $("#divAmountTotal").text("$"+amountTotal.toFixed(2));
            $("#amountTotal").val(amountTotal);
        }
    }

    var onSearchDestino = function (results, status) {
        if (status == 'OK') {
            contextAgregar.mapDestino.removeMarkers();
            var latlng = results[0].geometry.location;
            contextAgregar.mapDestino.setCenter(latlng.lat(), latlng.lng());
            contextAgregar.mapDestino.addMarker({
                lat: latlng.lat(),
                lng: latlng.lng(),
                draggable: true,
                fences: [contextAgregar.polygon],
                outside: function (marker, fence) {
                    $("#latitudDestino").val(marker.position.lat());
                    $("#longitudDestino").val(marker.position.lng());
                    GMaps.geocode({
                        latLng: marker.position,
                        callback: onSearchDestino
                    });
                }
            });
            $("#latitudDestino").val(latlng.lat());
            $("#longitudDestino").val(latlng.lng());
            if (results.length > 0) {
                $("#direccionMapaDestino").val(results[0].formatted_address);
            }
            updateKmTotal();
        }
    };

    var onSearchOrigen = function (results, status) {
        if (status == 'OK') {
            contextAgregar.mapOrigen.removeMarkers();
            var latlng = results[0].geometry.location;
            contextAgregar.mapOrigen.setCenter(latlng.lat(), latlng.lng());
            contextAgregar.mapOrigen.addMarker({
                lat: latlng.lat(),
                lng: latlng.lng(),
                draggable: true,
                fences: [contextAgregar.polygon],
                outside: function (marker, fence) {
                    $("#latitudOrigen").val(marker.position.lat());
                    $("#longitudOrigen").val(marker.position.lng());
                    GMaps.geocode({
                        latLng: marker.position,
                        callback: onSearchOrigen
                    });
                }
                //infoWindow: {
                //    content: '<p>"lat": {0}, "lng": {1}</p>'.format(latlng.lat(), latlng.lng())
                //}

            });
            $("#latitudOrigen").val(latlng.lat());
            $("#longitudOrigen").val(latlng.lng());
            if (results.length > 0) {
                $("#direccionMapaOrigen").val(results[0].formatted_address);
            }
        }
    };


    function initAgregarPedido() {

        contextAgregar.mapDestino = initMap("#mapDestino", "");
        contextAgregar.mapOrigen = initMap("#mapOrigen", "");
        contextAgregar.polygon = contextAgregar.mapDestino.drawPolygon({
            paths: [],
            strokeColor: '#BBD8E9',
            strokeOpacity: 1,
            strokeWeight: 3,
            fillColor: '#BBD8E9',
            fillOpacity: 0.6
        });


        jQuery("#direccionDestino, #direccionOrigen").keypress(function (event) {
            event = event || window.event;
            if (event.which == 13 || event.keyCode == 13) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
            }
        });
        jQuery("#btnDireccionOrigen").click(function (event) {
            event = event || window.event;
            if (event.preventDefault) {
                event.preventDefault();
            } else {
                event.returnValue = false;
            }
            GMaps.geocode({
                address: getDireccionOrigen(),
                callback: onSearchOrigen
            });
        });
        jQuery("#btnDireccionDestino").click(function (event) {
            event = event || window.event;
            if (event.preventDefault) {
                event.preventDefault();
            } else {
                event.returnValue = false;
            }
            contextAgregar.mapDestino.removeMarkers();
            GMaps.geocode({
                address: getDireccionDestino(),
                callback: onSearchDestino
            });
        });
    }


    function fillActivityOnMap() {
        var getActivityRequest = $.ajax({
            type: "GET",
            url: "/static/jsonData/routePedido.json",
            dataType: "json"
        });

        getActivityRequest.fail(function () {
            alert("No se pudo recuperar informacion del pedido, favor de reintentar.")
        });
        getActivityRequest.done(function (data) {
            var route = data ? data.route : undefined;
            contextEditar.route = route;
            if (route) {
                var waypoints = [], item;
                for (var index in route.activities.slice(0, 29)) {
                    item = route.activities[index];
                    waypoints.push({
                        stopover: false,
                        location: new google.maps.LatLng(item.lat, item.lng)
                    })
                }
                var lastActivity = route.activities[route.activities.length - 1];
                var destination = data.isComplete ? [route.destino.lat, route.destino.lng] : [lastActivity.lat, lastActivity.lng];

                var optionsRoute = {
                    origin: [route.origen.lat, route.origen.lng],
                    destination: destination,
                    travelMode: 'driving',
                    strokeColor: '#131540',
                    strokeOpacity: 0.6,
                    waypoints: waypoints,
                    strokeWeight: 7
                };
                contextEditar.mapRastreo.setCenter(lastActivity.lat, lastActivity.lng);
                contextEditar.mapRastreo.drawRoute(optionsRoute);
                contextEditar.mapRastreo.addMarker(
                    {
                        lat: route.origen.lat,
                        lng: route.origen.lng,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 8.5,
                            fillColor: "#DB6472",
                            fillOpacity: 1.0,
                            strokeWeight: 0.4
                        }
                    }
                );

                if (route.isComplete) {
                    contextEditar.mapRastreo.addMarker({
                        lat: route.destino.lat,
                        lng: route.destino.lng,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 8.5,
                            fillColor: "#88D58E",
                            fillOpacity: 1.0,
                            strokeWeight: 0.4
                        }
                    });
                } else {
                    var lastActivityMarker = new MarkerWithLabel({
                        position: new google.maps.LatLng(destination[0], destination[1]),
                        icon: ' ',
                        labelContent: '<i class="fa fa-motorcycle fa-4x" style="color:#E04F57;"></i>',
                        labelAnchor: new google.maps.Point(20, 10),
                        labelClass: "labels"
                    });
                    lastActivityMarker.setMap(contextEditar.mapRastreo.map);


                    setTimeout(function(){
                        contextEditar.mapRastreo.addMarker({
                            lat: route.destino.lat,
                            lng: route.destino.lng,
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 8.5,
                                fillColor: "#88D58E",
                                fillOpacity: 1.0,
                                strokeWeight: 0.4
                            },
                            animation:google.maps.Animation.DROP
                        });
                    }, 3000)
                }

            }
        });
    }

    function getColorByType(type) {
        switch (type) {
            case "Start":
                return "red";
            case "Info":
                return "blue";
            case "Warning":
                return "yellow";
            case "End":
                return "green";
        }
    }

    function loadActivities() {


        var $table = jQuery("#dtActivities");
        var oTable = $table.dataTable({ // here you can define a typical datatable settings from http://datatables.net/usage/options
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            pageLength: 20, // default record count per page
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                {
                    "className": '',
                    "orderable": false,
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return '<i class="fa fa-map-marker {2} fa-2x" data-lat="{0}" data-lng="{1}"></i>'.format(row.lat, row.lng, getColorByType(row.type));
                    }

                },
                {data: "mensaje"},
                {data: "dateTime"}
            ],
            order: [
                [0, "asc"]
            ], // set first column as a default sort by asc
            ajax: {
                url: "/static/jsonData/activityPedido.json", // ajax source
                type: "GET"
            }
        });

        $table.on('click', '.fa-map-marker', function (event) {
            event = event || window.event;
            var $target = $(event.target);

            var coordenates = $target.data();

            if (coordenates) {
                contextEditar.mapRastreo.setCenter(coordenates.lat, coordenates.lng);
            }
        });
    }

    var initEditarPedido = function () {
        contextEditar.mapRastreo = initMap("#mapRastreo");
        fillActivityOnMap();
        loadActivities();
    };


    return {
        consultar: function () {
            handleMapModals();
            initPickers();
            handlePedidos();

        },
        getMap: function () {
            return mapModal;
        },
        agregar: initAgregarPedido,
        editar: initEditarPedido
    }
}();