var Unidades = function () {
    var contextMonitoreo = {
        map: null,
        markers: []
    }

    var initMap = function (idSelector) {
        var map = new GMaps({
            div: idSelector,
            center: {
                lat: 19.432608,
                lng: -99.133208
            },
            zoom: 13
        });
        return map;
    };

    function clearMarkers() {
        contextMonitoreo.map.removeMarkers();
        contextMonitoreo.markers = [];
    }

    var onGetUnidades = function (data) {
        if (data && data.unidades) {
            var unidad;
            var baseOptions = {
                icon: ' ',
                labelContent: '<i class="fa fa-motorcycle fa-4x" style="color:#E04F57;"></i>',
                labelAnchor: new google.maps.Point(20, 10),
                labelClass: "labels"
            };
            for (var index in data.unidades) {
                unidad = data.unidades[index];
                contextMonitoreo.markers.push($.extend({
                    lat: unidad.lat,
                    lng: unidad.lng,
                    infoWindow: {
                        content: '<p><strong>Conductor</strong>: {0}</p> <p><strong>Unidad:</strong> {1}</p>'.format(unidad.conductor,unidad.idUnidad)
                    }
                }, baseOptions));

            }
            contextMonitoreo.map.addMarkers(contextMonitoreo.markers, true);
        }
    };
    var loadUnidades = function () {
        clearMarkers();
        var options = {
            url: "/static/jsonData/unidadesMonitor.json",
            type: "GET",
            datatype: "json"
        };
        var requestUnidades = $.ajax(options);


        requestUnidades.done(onGetUnidades);

        requestUnidades.fail(function () {
            alert("No se pudo recuperar la informacion de las unidades");
        });
    };
    var handleRefreshMap = function () {
        $("#mapRefresh").click(loadUnidades);
    };
    return {
        monitoreo: function () {
            contextMonitoreo.map = initMap('mapMonitoreo');
            loadUnidades();
            handleRefreshMap();
        }
    };
}();