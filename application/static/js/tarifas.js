"use strict";
var Tarifas = function () {
    var contextConsultar = {
        table: null
    };

    var initTarifasCliente = function () {
        var $table = jQuery("#tbTarifas");

        contextConsultar.table = $table.dataTable({ // here you can define a typical datatable settings from http://datatables.net/usage/options
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

            lengthMenu: [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            pageLength: 10, // default record count per page
            "language": {
                "url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "columns": [
                {
                    data: "idCliente"
                },
                {
                    data: "nombreCompleto"
                },
                {
                    data: "banderazo",
                    render: function (data, type, row, meta) {
                        return data.toMoney();
                    }
                },
                {
                    data: "km",
                    render: function (data, type, row, meta) {
                        return data.toMoney();
                    }
                },
                {
                    data: "tarifaFija",
                    render: function (data, type, row, meta) {
                        return data.toMoney();
                    }

                },
                {
                    data: "descuento",
                    render: function (data, type, row, meta) {
                        return "{0}%".format(data);
                    }
                },
                {
                    "className": '',
                    "orderable": false,
                    "data": "idCliente",
                    render: function (data, type, row, meta) {
                        return '<a class="primary-link editar-tarifa" data-idCliente="{0}">Editar</a>'.format(data);
                    }
                }
            ],
            ajax: {
                url: "/static/jsonData/tarifasByCliente.json", // ajax source
                type: "GET"
            },
            order: [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });
    };

    function actualizarTarifas($form, onSucessUpdate) {

        if (!$form) {
            return;
        }

        var data = $form.serialize();


        var options = {
            url: "/static/jsonData/tarifasEstandar.json",
            type: "GET",
            data: data,
            datatype: "json"
        };
        var requestInfoTarifas = $.ajax(options);

        requestInfoTarifas.done(onSucessUpdate);

        requestInfoTarifas.fail(function () {
            alert("No se pudieron actualizar las tarifas");
        });
    }

    var drawTarifasEstandar = function (data) {
        if (data) {
            var tarifas = data.tarifas;
            var target = $("#modalTarifas").data().target;
            $("#divBanderazo", "#divTarifasEstandar").text(tarifas.banderazo).data(tarifas.banderazo);
            $("#divKm", "#divTarifasEstandar").text(tarifas.km).data(tarifas.km);
            $("#divTarifaFija", "#divTarifasEstandar").text(tarifas.tarifaFija).data(tarifas.tarifaFija);
            $("#divDescuento", "#divTarifasEstandar").text(tarifas.descuento).data(tarifas.descuento);
            $("#modalTarifas").modal("hide");
        }
    };

    var drawTarifasCliente = function (data) {
        if (data) {
            var target = $("#modalTarifas").data().target;
            var row = target.parents('tr')[0];

            var oldData = contextConsultar.table.fnGetData(row);

            $.extend(oldData, data.tarifas);

            contextConsultar.table.fnUpdate(oldData, row);

            $("#modalTarifas").modal("hide");

        }
    };
    var showModalUpdateTarifa = function (data, $target) {

        if (data.idCliente) {
            $("#inpIdCliente", "#modalTarifas").val(data.idCliente);
        }
        $("#inpBanderazo", "#modalTarifas").val(data.banderazo);
        $("#inpKm", "#modalTarifas").val(data.km);
        $("#inpTarifaFija", "#modalTarifas").val(data.tarifaFija);
        $("#inpDescuento", "#modalTarifas").val(data.descuento);

        $("#modalTarifas").data({
            isClientUpdate: data.hasOwnProperty("idCliente"),
            target: $target
        });

        $("#modalTarifas").modal("show");


    };

    var onClickEditarTarifa = function (event) {
        event = event || window.event;
        var $target = $(event.target);
        var data;
        if (!$target.is("label")) {
            var row = $target.parents('tr')[0];
            data = contextConsultar.table.fnGetData(row);
        } else {
            data = {
                descuento: $("#divDescuento", "#divTarifasEstandar").data().descuento,
                tarifaFija: $("#divTarifaFija", "#divTarifasEstandar").data().tarifafija,
                banderazo: $("#divBanderazo", "#divTarifasEstandar").data().banderazo,
                km: $("#divKm", "#divTarifasEstandar").data().km
            };
        }

        showModalUpdateTarifa(data, $target);
    };

    var handleChangeTarifa = function () {

        $("#tbTarifas").on("click", '.editar-tarifa', onClickEditarTarifa);

        $("#frmTarifas", "#modalTarifas").submit(function (event) {
            event = event || window.event;
            Extensions.preventDefault(event);
            var $form = $(event.target);

            var data = $("#modalTarifas").data();
            var isClientUpdate = data.isClientUpdate;

            var onSuccessUpdate = isClientUpdate ? drawTarifasCliente : drawTarifasEstandar;

            actualizarTarifas($form, onSuccessUpdate);

        });

        $("#lblEditarTarifas").on("click", onClickEditarTarifa);


    };


    return {
        consultar: function () {
            initTarifasCliente();
            handleChangeTarifa();
        }
    };

}();